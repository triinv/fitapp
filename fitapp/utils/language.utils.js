import I18n from "react-native-i18n";
import en from "../config/language/en";
import et from "../config/language/et";
import ru from "../config/language/ru";

I18n.fallbacks = true;
I18n.defaultLocale = 'en';
I18n.locale = 'en';
I18n.translations = {
    en,
    et,
    ru
};

export const setLocale = (locale) => {
    I18n.locale = locale;
};

export const getCurrentLocale = () => I18n.locale;

export default I18n.translate.bind(I18n);
