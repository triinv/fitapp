import React from 'react';
import {Platform, SafeAreaView, Button, View} from 'react-native';
import {Icon} from 'react-native-elements';
import {createAppContainer} from '@react-navigation/native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {useDispatch} from 'react-redux';

import * as authActions from '../redux/actions/auth';
import Colors from '../assets/Colors';
import Login from '../components/views/Login';
import Register from '../components/views/Register.js';
import LaunchScreen from '../components/views/LaunchScreen';
import Dashboard from '../components/views/Dashboard.js';
import Favorites from '../components/views/Favorites.js';
import QRScanner from '../components/views/QRScanner';
import Categories from '../components/views/Categories.js';
import ExerciseCategory from '../components/views/ExerciseCategory.js';
import Exercise from '../components/views/Exercise';
import Activity from '../components/views/Activity.js';
import Profile from '../components/views/Profile.js';
import ChangePassword from '../components/views/ChangePassword';
import ChangeLanguage from '../components/views/ChangeLanguage';
import AddActivity from '../components/views/AddActivity';
import ForgotPassword from '../components/views/ForgotPassword.js';
import Tutorial from '../components/views/Tutorial.js';

const defaultStackNavOptions = {
  headerTransparent: true,
  headerTintColor: '#fff',
  headerTitle: 'A Screen',
};

const DashboardNavigator = createStackNavigator(
  {
    Dashboard: Dashboard,
    Favorites: Favorites,
    Exercise: Exercise,
    Profile: Profile,
    ChangePassword: ChangePassword,
    ChangeLanguage: ChangeLanguage,
    QRScanner: QRScanner,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  },
);

const CategoriesNavigator = createStackNavigator(
  {
    Categories: Categories,
    ExerciseCategory: ExerciseCategory,
    QRScanner: QRScanner,
    Exercise: Exercise,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  },
);

const ActivityNavigator = createStackNavigator(
  {
    Activity: Activity,
    Exercise: Exercise,
    AddActivity: AddActivity,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  },
);

const tabScreenConfig = {
  Dashboard: {
    screen: DashboardNavigator,
    navigationOptions: {
      tabBarLabel: 'Feed',
      tabBarIcon: tabInfo => {
        return <Icon name="subject" size={25} color={tabInfo.tintColor} />;
      },
    },
  },
  Categories: {
    screen: CategoriesNavigator,
    navigationOptions: {
      tabBarLabel: 'Exercises',
      tabBarIcon: tabInfo => {
        return (
          <Icon name="fitness-center" size={25} color={tabInfo.tintColor} />
        );
      },
    },
  },
  Activity: {
    screen: ActivityNavigator,
    navigationOptions: {
      tabBarLabel: 'Activity',
      tabBarIcon: tabInfo => {
        return <Icon name="event" size={25} color={tabInfo.tintColor} />;
      },
    },
  },
};

const BottomTabNavigator =
  Platform.OS === 'android'
    ? createMaterialBottomTabNavigator(tabScreenConfig, {
        activeTintColor: Colors.accentColorRed,
        shifting: false,
        barStyle: {
          backgroundColor: Colors.primaryColor,
        },
      })
    : createBottomTabNavigator(tabScreenConfig, {
        tabBarOptions: {
          activeTintColor: Colors.primaryColor,
        },
      });

const LoginNavigator = createStackNavigator(
  {
    Tutorial: Tutorial,
    Register: Register,
    Login: Login,
    ForgotPassword: ForgotPassword,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  },
);

const MainNavigator = createSwitchNavigator(
  {
    Tutorial: LoginNavigator,
    Dashboard: BottomTabNavigator,
  },
  {
    contentOptions: {
      activeTintColor: Colors.primaryColor,
    },
  },
);
export default createAppContainer(MainNavigator);
