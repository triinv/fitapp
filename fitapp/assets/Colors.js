export default {
    primaryColor: '#0072BC',
    accentColorYellow: '#ffb612',
    accentColorRed: '#c41039',
    textColor: '#002d4d'
};
