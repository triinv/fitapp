import React from 'react';
import {Platform} from 'react-native';
import {HeaderButton} from 'react-navigation-header-buttons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';

import Colors from '../../assets/Colors';

const HeaderIconButton = props => {
    return <HeaderButton {...props} IconComponent={Ionicons} iconSize={23}
                         color={Platform.OS === 'android' ? '#fff' : Colors.primaryColor}/>;
};

export default HeaderIconButton;
