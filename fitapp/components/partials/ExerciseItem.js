import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Colors from '../../assets/Colors';

const ExerciseItem = props => {

    return (
        <View style={styles.exerciseListItem}>
            <View style={{flexDirection: 'row'}}>
                <View style={{...styles.row, width: '70%'}}>
                    <Image
                        source={{uri: props.machineImage}}
                        style={styles.exerciseListImage}
                    />
                    <View style={styles.exerciseListTitleContainer}>
                        <View style={{alignContent: 'center',borderBottomColor: Colors.textColor,
                            borderBottomWidth: 1,
                        }}>
                        <Text style={styles.exerciseTitle} numberOfLines={2}>
                            {props.title}
                        </Text>
                        </View>
                        <Text style={styles.machineTitle}>{props.machineTitle}</Text>

                    </View>
                </View>

                <View style={{...styles.exerciseQRContainer, width: '30%', alignItems: 'center', height: '100%'}}>
                    <TouchableOpacity onPress={props.onSelecQR}>
                        <Text style={{
                            fontSize: 20,
                            fontWeight: '600',
                            alignSelf: 'center',
                            textTransform: 'uppercase',
                        }}>nr{props.machineNr}</Text>
                        <Image style={styles.qrIcon}
                               source={require('../../assets/images/qr_code.png')}/>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    exerciseListItem: {
        height: 100,
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 10,
        overflow: 'hidden',
        marginVertical: '1%',
        justifyContent: 'center',
        alignContent: 'center',
    },
    exerciseListImage: {
        width: 60,
        height: 60,
        marginHorizontal: 10,
        alignSelf: 'center',
    },
    row: {
        flexDirection: 'row',
    },
    exerciseQRContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
    },
    exerciseListTitleContainer: {
        paddingVertical: 25,
        flex: 1,
    },
    title: {
        fontSize: 18,
        color: Colors.textColor,
        textAlign: 'left',
    },
    qrIcon: {
        width: 60,
        height: 60,
    },
    exerciseTitle:{
        color: Colors.textColor,
        fontSize: 18,
        paddingBottom: '5%'
    },
    machineTitle:{
        color: Colors.textColor,
        fontSize: 14,
        paddingTop: '5%'
    
    
    },
});

export default ExerciseItem;
