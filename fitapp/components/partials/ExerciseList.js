import React, {Component} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';

import ExerciseItem from './ExerciseItem';

const ExerciseList = props => {
    const favoriteExercises = useSelector(state => state.exercises.favoriteExercises);

    const renderExerciseItem = itemData => {
        const isFavorite = favoriteExercises.find(exercise => exercise.id === itemData.item.id);

        return (
            <ExerciseItem
                title={itemData.item.title}
                machineImage={itemData.item.machineImageUrl}
                image={itemData.item.imageUrl}
                machineTitle={itemData.item.machineTitle}
                machineNr={itemData.item.machineNr}
                onSelecQR={() => {
                    props.navigation.navigate({
                        routeName: 'QRScanner',
                        //params: {
                        //    exerciseId: itemData.item.id,
                        //    exerciseTitle: itemData.item.title,
                        //    isFav: isFavorite,
                        //},
                    });
                }}
            />
        );
    };

    return (
        <View style={styles.list}>
            <FlatList
                data={props.listData}
                keyExtractor={(item, index) => item.id}
                renderItem={renderExerciseItem}
                style={{width: '100%'}}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
    },
});

export default ExerciseList;
