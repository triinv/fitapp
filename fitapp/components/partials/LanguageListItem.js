import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import { Icon } from "react-native-elements";
import Colors from '../../assets/Colors';

class LanguageListItem extends React.Component {

    render() {
        return (
            <TouchableOpacity style={styles.languageListItemContainer} onPress={() => this.props.onLanguagePress(this.props.locale)}>
                <View style={styles.languageListItem}>
                    <Text style={[styles.languageTitle, (this.props.isSelected && styles.selectedLanguage)]}>{this.props.name}</Text>
                    <Icon
                        iconStyle={[styles.hiddenIcon, this.props.isSelected && styles.selectedLanguage]}
                        name="check-circle"
                        size={30}
                    />
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    languageListItemContainer:{
        flexDirection: 'row',
        paddingVertical: 19.6,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
    },
    languageListItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 10
    },
    languageTitle: {
        paddingLeft: '2%',
        alignSelf: 'center',
        color: Colors.textColor
    },
    selectedLanguage: {
        color: Colors.primaryColor,
    },
    hiddenIcon:{
        color: 'transparent',
    },
})


export default LanguageListItem;
