import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, ImageBackground} from 'react-native';

const ExerciseCategoryGrid = props => {
    return (
        <TouchableOpacity style={styles.gridItem} onPress={props.onSelect}>
                <ImageBackground
                    source={{uri: props.color}}
                    style={styles.gridContainer}
                    imageStyle={{ borderRadius: 10 }}
                >
                <Text style={styles.title}>{props.title}</Text>
                </ImageBackground>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 150,
    },
    gridContainer: {
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 10,
        padding: 10,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    title: {
        fontSize: 22,
        fontWeight: '700',
        color: '#fff',
    },
});


export default ExerciseCategoryGrid;
