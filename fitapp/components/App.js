import React, {useState} from 'react';
import Routes from '../navigation/Routes';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import exerciseReducer from '../redux/reducers/exercises';
import authReducer from '../redux/reducers/auth';
import ReduxThunk from 'redux-thunk';

console.disableYellowBox = true;

const rootReducer = combineReducers({
    exercises: exerciseReducer,
    auth: authReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));


export default function App() {
    return (
        <Provider store={store}>
            <Routes/>
        </Provider>

    );
}
