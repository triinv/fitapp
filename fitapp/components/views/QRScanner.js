import React, {useState} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  Linking,
  View,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import firebase from '@react-native-firebase/app';

var array = [];

export default class QRScanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scanner: '',
      video: '',
      information: '',
      exercises: '',
      tips: '',
    };
  }
  componentDidMount() {
    this.setState({
      video: null,
      information: null,
      exercises: null,
      scanner: '',
      tips: null,
    });
  }
  onSuccess = e => {
    Linking.openURL(e.data).catch(err => console.log('An error occured', err));
  };
  onRead = e => {
    console.log('data', e);
    var code = e.data;
    this.setState({scanner: code});
    var that = this;
    var ref = firebase.database().ref('machines');
    ref.once('value', function(snapshot) {
      var data = snapshot.child(that.state.scanner).child('video_url');
      var data1 = snapshot.child(that.state.scanner).child('exercise_name');
      var data2 = snapshot.child(that.state.scanner).child('instruction');
      var data3 = snapshot.child(that.state.scanner).child('tips');
      console.log('machine data is that. DATA....', data);
      console.log('information about machine.DATA1...', data1);
      console.log('information about machine DATA 2....', data2);
      console.log('information about machine DATA 3....', data3);
      that.setState({video: data, information: data1, exercises: data2, tips: data3});
      that.router(data, data1, data2, data3);
      array.push({video: data, information: data1, exercises: data2, tips: data3});
    });
  };
  router(data, data1, data2, data3) {
    this.props.navigation.navigate('Exercise', {
      video: data,
      exer: data1,
      info: data2,
      tips: data3,
    });
  }
  
  componentDidMount() {
    console.log('MACHINE NAME', this.state.machine_);
  }
  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_bg.png')}>
        <QRCodeScanner
          onRead={this.onRead}
          onSuccess={this.onSuccess}
          //flashMode={RNCamera.Constants.FlashMode.torch}
          bottomContent={
            <View style={styles.qrTopContainer}>
              <Text style={styles.textStyle}>
                Hold your device over a QR Code so that it’s clearly visible
                within your phone’s screen.
              </Text>
            </View>
          }
       
        />
      </ImageBackground>
    );
  }
}
QRScanner.navigationOptions = navData => {
  return {
    headerTitle: 'QR Scan',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  QRcenterText: {
    fontSize: 18,
    color: '#fff',
    paddingTop: '20%',
  },
  qrTopContainer:{
    height: '80%',
    width: '92%',
    justifyContent: 'flex-end',
  },
  textStyle: {
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'flex-end',
    fontSize: 16,
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
