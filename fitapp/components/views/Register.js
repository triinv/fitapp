import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Colors from '../../assets/Colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import {userData} from '../../redux/actions/auth.js';
//import Input from '../partials/Input.js';
import {bindActionCreators} from 'redux';
import PasswordInputText from 'react-native-hide-show-password-input';
import {TextField} from 'react-native-material-textfield';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      Password: '',
    };
  }

  register_user() {
    let {username, actions} = this.props;
    auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.Password)
      .then(() => {
        console.log('User account created & signed in!');
        alert('Account created and signed in!');
        actions.userData(this.state.email);
        this.props.navigation.navigate('Dashboard');
      })
      .catch(error => {
        alert(
          'Make sure that you use correct, not already in use e-mail and that password is at least 5 characters',
        );
      });
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_bg.png')}>
        <View style={styles.mainContainer}>
          <KeyboardAwareScrollView
            keyboardShouldPersistTaps="always"
            contentContainerStyle={{flexGrow: 1}}
            enableOnAndroid={false}
            scrollEnabled>
            <View style={styles.topContainer}>
              <View style={styles.loginImgContainer}>
                <Image
                  resizeMode="contain"
                  style={{
                    width: '100%',
                    height: '100%',
                  }}
                  source={require('../../assets/images/impuls_logo.png')}
                />
              </View>
            </View>
            <View style={styles.middleContainer}>
              <View style={{width: '80%'}}>
                <TextField
                  id="email"
                  label="Email"
                  keyboardType="email-address"
                  required
                  email
                  textColor={Colors.textColor}
                  tintColor={Colors.textColor}
                  baseColor={Colors.textColor}
                  labelTextStyle={styles.label}
                  inputStyle={styles.input}
                  autoCapitalize="none"
                  errorText="Please enter a valid email address."
                  //onInputChange={inputChangeHandler}
                  initialValue=""
                  onChangeText={value => this.setState({email: value})}
                  leftIcon={{
                    type: 'font-awesome',
                    name: 'user',
                    color: Colors.textColor,
                    marginLeft: -20,
                    padding: '5%',
                  }}
                />
                <PasswordInputText
                  id="password"
                  label="Password"
                  keyboardType="default"
                  secureTextEntry
                  required
                  minLength={5}
                  textColor={Colors.textColor}
                  tintColor={Colors.textColor}
                  baseColor={Colors.textColor}
                  labelTextStyle={styles.label}
                  inputStyle={styles.input}
                  autoCapitalize="none"
                  errorText="Please enter a valid password."
                  initialValue=""
                  onChangeText={value => this.setState({Password: value})}
                  leftIcon={{
                    type: 'font-awesome',
                    name: 'lock',
                    color: Colors.textColor,
                    marginLeft: -20,
                    padding: '5%',
                  }}
                />
              </View>
            </View>

            <View style={styles.bottomContainer}>
              <View style={styles.buttonContainer}>
                <Button
                  buttonStyle={styles.loginButtonStyle}
                  titleStyle={styles.buttonText}
                  title={'Register'}
                  onPress={() => this.register_user()}
                />
              </View>
              <View style={styles.buttonContainer}>
                <Button
                  buttonStyle={styles.loginOptionButtonStyle}
                  titleStyle={styles.whiteText}
                  title={'Already have an account? Log In'}
                  onPress={() => this.props.navigation.navigate('Login')}
                />
                <View style={styles.center}>
                  <Text style={styles.whiteText}>
                    By signing up you agree to our
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity
                      onPress={() => {
                        Linking.openURL(
                          'https://triinvaht.wixsite.com/impuls/terms-conditions',
                        );
                      }}>
                      <Text style={styles.whiteTextUnderline}>
                        Terms & Conditions
                      </Text>
                    </TouchableOpacity>
                    <Text style={styles.whiteText}> and </Text>
                    <TouchableOpacity
                      onPress={() => {
                        Linking.openURL(
                          'https://triinvaht.wixsite.com/impuls/privacy-policy',
                        );
                      }}>
                      <Text style={styles.whiteTextUnderline}>
                        Privacy Policy
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </ImageBackground>
    );
  }
}

Register.navigationOptions = navData => {
  return {
    header: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  middleContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 1)',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  bottomContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  keyboardAwareLogin: {
    flex: 1,
  },
  loginImgContainer: {
    width: '70%',
  },
  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
  loginOptionButtonStyle: {
    width: 300,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    marginBottom: 20,
  },
  whiteText: {
    fontSize: 16,
    color: '#fff',
  },
  whiteTextUnderline: {
    fontSize: 14,
    color: '#fff',
    textDecorationLine: 'underline',
  },
  label: {
    color: Colors.textColor,
    textTransform: 'uppercase',
    fontWeight: '400',
  },
  input: {
    color: Colors.textColor,
    fontSize: 16,
  },
});

const mapStateToProps = function(state) {
  return {
    username: state.username.username,
  };
};
const ActionCreators = Object.assign({}, {userData});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
