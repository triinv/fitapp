import auth from '@react-native-firebase/auth';
import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  ImageBackground,
  Dimensions,
} from 'react-native';
import {Button} from 'react-native-elements';

import Swiper from 'react-native-swiper'
import Colors from '../../assets/Colors.js';
import translate from '../../utils/language.utils.js';

export default class Tutorial extends React.Component {

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.middleContainer}>
            
            <View>
              <Swiper style={styles.wrapper} showsButtons={true} loop={false}>
                
                <View style={styles.slide}>
                  <Text style={styles.text}>Filter exercises by muscle groups</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-01.png')}/>
                </View>
                
                <View style={styles.slide}>
                  <Text style={styles.text}>Find equipment you want to use</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-04.png')}/>
                </View>
  
                <View style={styles.slide}>
                  <Text style={styles.text}>Scan QR code on the equipment</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-03.png')}/>
                </View>
  
                <View style={styles.slide}>
                  <Text style={styles.text}>See exercise instructions and add favorites</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-07.png')}/>
                </View>
  
                <View style={styles.slide}>
                  <Text style={styles.text}>Use quick scan</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-02.png')}/>
                </View>
                
                <View style={styles.slide}>
                  <Text style={styles.text}>See all your favorite exercises</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-08-07.png')}/>
                </View>
                
                <View style={styles.slide}>
                  <Text style={styles.text}>Keep track of your workouts</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-06.png')}/>
                </View>
  
                <View style={styles.slide}>
                  <Text style={styles.text}>Log new workout</Text>
                  <Image style={{width: 200, height: 360}} source={require('../../assets/tutorial/fitapp_tutorial-05.png')}/>
                </View>
                
              </Swiper>
            </View>

          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.buttonContainer}>
              <Button
                buttonStyle={styles.loginButtonStyle}
                titleStyle={styles.buttonText}
                title={translate('startExploring')}
                //title={'startExploring'}
                onPress={() => this.props.navigation.navigate('Login')}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

Tutorial.navigationOptions = navData => {
  return {
    headerTitle: '',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,

  },
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    paddingTop: '22%',
  },
  middleContainer: {
    flex: 8,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomContainer: {
    flex: 1,
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrow: {
    width: 50,
    height: 50,
  },

  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
  wrapper: {},
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    paddingBottom: '5%',
    textAlign: 'center',
    paddingHorizontal: '5%'
  }

});
