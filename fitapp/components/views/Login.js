import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  Dimensions,
  Image,
  ActivityIndicator,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Colors from '../../assets/Colors';
import {Button, Input} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import {userData} from '../../redux/actions/auth.js';
import {bindActionCreators} from 'redux';
import PasswordInputText from 'react-native-hide-show-password-input';
import {TextField} from 'react-native-material-textfield';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      Password: '',
      spinner: false,
    };
  }
  login_user() {
    let {username, actions} = this.props;
    this.setState({spinner: true});
    auth()
      .signInWithEmailAndPassword(this.state.email, this.state.Password)
      .then(() => {
        var data = this.state.email;
        actions.userData(data);
        this.props.navigation.navigate('Dashboard');
        console.log(username);
      })
      .catch(error => {
        alert('Wrong e-mail or password');
        this.setState({spinner: false});
      });
  }

  render() {
    const {username, userid} = this.props;
    const screenHeight = Math.round(Dimensions.get('window').height) / 2;
    if (this.state.splinner == true) {
      return (
        <ActivityIndicator
          color='#fff'
          style={{paddingVertical: screenHeight, backgroundColor: '#0072BC'}}
        />
      );
    } else {
      return (
        <ImageBackground
          style={styles.backgroundImage}
          source={require('../../assets/images/fitapp_bg.png')}>
          <View style={styles.mainContainer}>
            <KeyboardAwareScrollView
              keyboardShouldPersistTaps="always"
              contentContainerStyle={{flexGrow: 1}}
              enableOnAndroid={false}
              scrollEnabled>
              <View style={styles.topContainer}>
                <View style={styles.loginImgContainer}>
                  <Image
                    resizeMode="contain"
                    style={{
                      width: '100%',
                      height: '100%',
                    }}
                    source={require('../../assets/images/impuls_logo.png')}
                  />
                </View>
              </View>
              <View style={styles.middleContainer}>
                <View style={{width: '80%'}}>
                  <TextField
                    id="email"
                    label="Email"
                    keyboardType="email-address"
                    required
                    email
                    textColor={Colors.textColor}
                    tintColor={Colors.textColor}
                    baseColor={Colors.textColor}
                    labelTextStyle={styles.label}
                    inputStyle={styles.input}
                    autoCapitalize="none"
                    errorText="Please enter a valid email address."
                    initialValue=""
                    onChangeText={value => this.setState({email: value})}
                    leftIcon={{
                      type: 'font-awesome',
                      name: 'user',
                      color: Colors.textColor,
                      marginLeft: -20,
                      padding: '5%',
                    }}
                  />
                  <PasswordInputText
                    id="password"
                    label="Password"
                    keyboardType="default"
                    secureTextEntry
                    required
                    minLength={5}
                    textColor={Colors.textColor}
                    tintColor={Colors.textColor}
                    baseColor={Colors.textColor}
                    labelTextStyle={styles.label}
                    inputStyle={styles.input}
                    autoCapitalize="none"
                    errorText="Please enter a valid password."
                    //onInputChange={inputChangeHandler}
                    initialValue=""
                    onChangeText={value => this.setState({Password: value})}
                    leftIcon={{
                      type: 'font-awesome',
                      name: 'lock',
                      color: Colors.textColor,
                      marginLeft: -20,
                      padding: '5%',
                    }}
                  />
                </View>
              </View>

              <View style={styles.bottomContainer}>
                <View style={styles.buttonContainer}>
                  <Button
                    buttonStyle={styles.loginButtonStyle}
                    titleStyle={styles.buttonText}
                    title={'Sign In'}
                    onPress={() => this.login_user()}
                  />
                </View>
                <View style={styles.buttonContainer}>
                  <Button
                    buttonStyle={styles.loginOptionButtonStyle}
                    titleStyle={styles.loginOptionButtonTextStyle}
                    title={"Don't have an account yet? Register"}
                    onPress={() => this.props.navigation.navigate('Register')}
                  />
                  <Button
                    buttonStyle={styles.loginOptionButtonStyle}
                    titleStyle={styles.loginOptionButtonTextStyle}
                    title={'Forgot Password?'}
                    onPress={() =>
                      this.props.navigation.navigate('ForgotPassword')
                    }
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </ImageBackground>
      );
    }
  }
}

Login.navigationOptions = navData => {
  return {
    header: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  middleContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 1)',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  bottomContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  keyboardAwareLogin: {
    flex: 1,
  },
  loginImgContainer: {
    width: '70%',
  },

  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
  loginOptionButtonStyle: {
    width: 300,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    marginBottom: 20,
  },
  loginOptionButtonTextStyle: {
    fontSize: 16,
  },
  label: {
    color: Colors.textColor,
    textTransform: 'uppercase',
    fontWeight: '400',
  },
  input: {
    color: Colors.textColor,
    fontSize: 16,
  },
});

const mapStateToProps = function(state) {
  return {
    username: state.username.username,
  };
};

const ActionCreators = Object.assign({}, {userData});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
