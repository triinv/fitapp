import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, ImageBackground, Dimensions, Image, Platform} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

import translate from '../../utils/language.utils';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDate: new Date(),
      markedDate: moment(new Date()).format('YYYY-MM-DD'),
    };
  }

  render() {
    const today = this.state.currentDate;
    const day = moment(today).format('dddd');
    const date = moment(today).format('MMMM D, YYYY');
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <View style={styles.mainContainer}>
          <View style={styles.container}>
            <View style={styles.dateContainer}>
              <Text style={styles.bigTitle}>{day}</Text>
              <Text style={styles.title}>{date}</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.favoritesGridItem}
            onPress={() => {
              this.props.navigation.navigate({
                routeName: 'Favorites',
              });
            }}>
            <ImageBackground
              style={styles.favoritesImage}
              imageStyle={{borderRadius: 10}}
              source={require('../../assets/images/gym.jpg')}>
              <View style={styles.favoritesTextContainer}>
                <Text style={styles.title}>
                  {translate('favoriteExercise')}
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>

          <View style={{flex: 2, width: '100%'}}>
            <TouchableOpacity
              style={styles.qrGridItem}
              onPress={() => {
                this.props.navigation.navigate({
                  routeName: 'QRScanner',
                });
              }}>
              <View style={styles.quickQrContainer}>
                <Image
                  style={styles.quickQrIcon}
                  source={require('../../assets/images/qr_code_icon.png')}
                />
                <View style={styles.qrTitleContainer}>
                  <Text style={styles.title}>Quick QR scan</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

Dashboard.navigationOptions = ({navigation, screenProps}) => {
  return {
    headerTitle: 'Home',
    headerBackTitle: null,
    headerRight: (
      <Button
        icon={<Icon name="account-circle" size={30} color="#fff" />}
        title={' '}
        type={'clear'}
        onPress={() => {
          navigation.navigate('Profile');
        }}
      />
    ),
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
  },
  dateContainer: {
    width: Dimensions.get('window').width,
    marginLeft: '8%',
    paddingVertical: '2%',
  },
  favoritesGridItem: {
    margin: 15,
    flex: 2,
    width: '92%',
  },
  favoritesImage: {
    flex: 1,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    padding: 10,
  },
  favoritesTextContainer: {
    flex: 1,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    padding: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  bigTitle: {
    fontSize: 32,
    fontWeight: '700',
    color: '#fff',
  },
  title: {
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
  },
  qrGridItem: {
    margin: 15,
    height: 120,
    width: '92%',
    borderRadius: 10,
  },
  quickQrContainer: {
    flex: 1,
    //borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    padding: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'rgba(000,000,000,0.2)',
  },
  quickQrIcon: {
    alignSelf: 'center',
    width: 80,
    height: 80
  },
  qrTitleContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});
