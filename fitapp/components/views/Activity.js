import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Dimensions,
  ScrollView,
} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {Button} from 'react-native-elements';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import {userName} from '../../redux/actions/auth.js';
import {bindActionCreators} from 'redux';

import Colors from '../../assets/Colors';


class Activity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      selectedDate: '',
      ExersiesDated: '',
      marked: null,
      ExersiesDate: '',
      selectedDay: '',
      Uid: '',
    };
  }
  componentDidMount() {
    this.get_firebase();
    auth().onAuthStateChanged(user => {
      this.setState({
        Uid: user.uid,
      });
    });
  }
  get_firebase() {
    var array = [];
    var arr = [];
    var that = this;
    var ref = firebase.database().ref('/activity_calender');
    ref.on(
      'value',
      function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var childData = childSnapshot.val();
          //console.log(childSnapshot.val().date)
          if (childSnapshot.val().user_id == that.state.Uid) {
            arr.push(childSnapshot.val().date);
            array.push({
              date: childSnapshot.val().date,
              exercise: childSnapshot.val().exersises_name,
            });
            //  console.log(childSnapshot.val().date)
          }
        });
        // console.log('new data', arr);
        that.setState({selectedDate: arr});
        that.setState({ExersiesDated: array});
        console.log('newSS', that.state.ExersiesDated);
        console.log('newSS DATEDD', that.state.selectedDate);
        var obj = that.state.selectedDate.reduce(
          (c, v) => Object.assign(c, {[v]: {selected: true, marked: true}}),
          {},
        );
        that.setState({marked: obj});
      },
      function(error) {
        console.log('Error:' + error.code);
      },
    );
  }
  render() {
    const {username, userid} = this.props;
    const {navigation} = this.props;
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <ScrollView
          style={{marginTop: '22%', width: Dimensions.get('window').width}}>
          <View style={styles.mainContainer}>
            <Calendar
              // Collection of dates that have to be marked. Default = {}
              onDayPress={day => {
                console.log('selected days', day.dateString);
                for (let j = 0; j < this.state.ExersiesDated.length; j++) {
                  if (day.dateString == this.state.ExersiesDated[j].date) {
                    console.log('yes', this.state.ExersiesDated[j].exercise);
                    this.setState({
                      ExersiesDate: this.state.ExersiesDated[j].exercise,
                      selectedDay: day.dateString,
                    });
                  }
                }
                this.setState({date: day.dateString});
              }}
              markedDates={this.state.marked}
              firstDay={1}
              theme={{
                textSectionTitleColor: Colors.textColor,
                selectedDayBackgroundColor: Colors.accentColorYellow,
                selectedDayTextColor: '#ffffff',
                todayTextColor: Colors.accentColorRed,
                dayTextColor: Colors.textColor,
                arrowColor: Colors.primaryColor,
                monthTextColor: Colors.primaryColor,
                indicatorColor: Colors.primaryColor,
              }}
            />
          </View>
          
          <View style={styles.markedWorkoutContainer}>
            <View>
              <Text style={{marginLeft: 10, fontSize: 26, color: '#fff'}}>{this.state.ExersiesDate}</Text>
            </View>
            
            <View style={{flexDirection: 'row'}}>
              <Text style={{marginLeft: 10, fontSize: 16, color: '#fff', fontWeight: '600'}}>{this.state.selectedDay}</Text>
            </View>
           
          </View>

          <View style={styles.bottomContainer}>
            <View
              style={{
                flex: 1,
                width: '60%',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: '10%'
              }}>
              <View style={styles.buttonContainer}>
  
              <Button
                buttonStyle={styles.loginButtonStyle}
                titleStyle={styles.buttonText}
                title="Add new"
                onPress={() =>
                  this.props.navigation.navigate({
                    routeName: 'AddActivity',
                  })
                }
              />
            </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

Activity.navigationOptions = navData => {
  return {
    headerTitle: 'Activity',
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    width: '100%',
  },
  bottomContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  dateTitle: {
    color: '#fff',
    fontSize: 18,
    marginVertical: '5%',
  },
  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
  markedWorkoutContainer: {
    height: 120,
    width: '92%',
    margin: 15,
    //borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    padding: 10,
    justifyContent: 'space-around',
    backgroundColor: 'rgba(000,000,000,0.2)',
  },
});

const mapStateToProps = function(state) {
  return {
    username: state.username.username,
  };
};
const ActionCreators = Object.assign({}, {userName});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Activity);
