import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import firebase from '@react-native-firebase/app';
import Colors from '../../assets/Colors';

export default class ExerciseCategory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data_: this.props.navigation.state.params.data,
      user: '',
      date: '',
      selected_exersise: '',
      exersises: '',
      Image_url: '',
      exer: '',
    };
  }
  componentDidMount() {
    console.log('data_', this.state.data_);
    var that = this;
    var arr = [];
    var ref = firebase.database().ref('exercise_name');
    ref.once('value', function(snapshot) {
      var data1 = snapshot.child(that.state.data_);
      var key = Object.keys(data1);
      data1.forEach(function(userSnapshot) {
        var username = userSnapshot.val();
        console.log('uuduuu', username.exercise_name);
        if (username.exercise_name == undefined) {
          console.log('no user identified');
        } else {
          arr.push({
            exercise: username.exercise_name,
            machine: username.machine_name,
            qr: username.qr_code,
            image: username.exercise_image,
          });
        }
      });
      that.setState({exer: arr});
      console.log('NEW DATA', that.state.exer);
    });
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <View style={styles.mainContainer}>
          <FlatList
            data={this.state.exer}
            style={{width: '100%', height: 200}}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <View style={styles.exerciseListItem}>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{...styles.row, width: '70%'}}>
                      <Image
                        source={{uri: item.image}}
                        style={styles.exerciseListImage}
                      />
                      <View style={styles.exerciseListTitleContainer}>
                        <View
                          style={{
                            alignContent: 'center',
                            borderBottomColor: Colors.textColor,
                            borderBottomWidth: 1,
                          }}>
                          <Text style={styles.title} numberOfLines={1}>
                            {item.exercise}
                          </Text>
                        </View>
                        <Text style={styles.title} numberOfLines={1}>
                          {item.machine}
                        </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        ...styles.exerciseQRContainer,
                        width: '30%',
                        alignItems: 'center',
                        height: '100%',
                      }}>
                      <TouchableOpacity
                        onPress={() => {
                          //this.get_machine(item.qr);
                          this.props.navigation.navigate('QRScanner');
                        }}>
                        <Text
                          style={{
                            fontSize: 20,
                            fontWeight: '600',
                            alignSelf: 'center',
                            textTransform: 'uppercase',
                          }}>
                          nr {item.qr}
                        </Text>
                        <Image
                          style={styles.qrIcon}
                          source={require('../../assets/images/qr_code.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            }}
          />
        </View>
      </ImageBackground>
    );
  }
}
ExerciseCategory.navigationOptions = navData => {
  return {
    headerTitle: 'Exercises',
  };
};
const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
    marginBottom: '-22%',
  
  },
  exerciseListItem: {
    height: 100,
    width: '92%',
    backgroundColor: '#fff',
    //borderRadius: 10,
    overflow: 'hidden',
    marginVertical: '1%',
    justifyContent: 'center',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  exerciseListImage: {
    width: 60,
    height: 60,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  exerciseQRContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  exerciseListTitleContainer: {
    paddingVertical: 25,
    flex: 1,
  },
  title: {
    fontSize: 18,
    color: Colors.textColor,
    textAlign: 'left',
    paddingVertical: 10,
  },
  qrIcon: {
    width: 60,
    height: 60,
  },
  exerciseTitle: {
    color: Colors.textColor,
    fontSize: 18,
    paddingBottom: '5%',
  },
  machineTitle: {
    color: Colors.textColor,
    fontSize: 14,
    paddingTop: '5%',
  },
});
