/**
 * Settings modal for choosing a language
 */

import React from 'react';
import {Dimensions, ImageBackground, StyleSheet, View} from 'react-native';
import {connect} from "react-redux";
import {changeLanguage} from "../../redux/actions/lang";
import translate from '../../utils/language.utils';
import LanguageListItem from '../partials/LanguageListItem';


const languages = [
    {
        locale: 'en',
        name: 'English',
    },
    {
        locale: 'et',
        name: 'Eesti',
    },
    {
        locale: 'ru',
        name: 'Русский',
    },
];

const mapStateToProps = state => {
    return {settings: state.settings};
};

function mapDispatchToProps(dispatch) {
    return {
        changeLanguage: settings => dispatch(changeLanguage(settings)),
    };
}

class ChangeLanguage extends React.Component {

    constructor() {
        super();
        this.state = {
            selectedIndex: 0,
        };
        this.handleLangChange = this.handleLangChange.bind(this);
    }

    handleLangChange(locale) {
        this.props.changeLanguage({language: locale});
        this.setState({selectedIndex: languages.findIndex(x => x.locale === locale)});
    }

    componentDidMount() {
        this.setState({
            selectedIndex: languages.findIndex(x => x.locale === this.props.language),
        });
    }

    render() {

        return (
            <ImageBackground
                style={styles.backgroundImage}
                source={require('../../assets/images/fitapp_simple_bg.png')}>
                <View style={styles.mainContainer}>
                    <View style={styles.middleContainer}>

                        <View style={{width: '90%'}}>
                            {languages.map((language, index) => (<LanguageListItem
                                onLanguagePress={this.handleLangChange}
                                isSelected={this.state.selectedIndex === index}
                                key={language.locale}
                                locale={language.locale}
                                name={language.name}
                            />))}
                        </View>
                    </View>
                </View>
            </ImageBackground>


        );
    }
}

ChangeLanguage.navigationOptions = navData => {
    return {
        headerTitle: 'Language',
        headerBackTitle: null,
    };
};


const styles = StyleSheet.create({
    backgroundImage: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mainContainer: {
        flex: 1,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        paddingTop: '22%',
    },
    middleContainer: {
        marginTop: 20,
        width: '95%',
        height: 210,
        paddingVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    },

});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLanguage);

