import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import Colors from '../../assets/Colors';

export default class Favorites extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      date: '',
      selected_exersise: '',
      exersises: '',
      Uid: '',
    };
  }
  componentDidMount() {
    this.get_faviorate();
    auth().onAuthStateChanged(user => {
      this.setState({
        Uid: user.uid,
      });
    });
  }
  get_faviorate() {
    var arr = [];
    var that = this;
    var ref = firebase.database().ref('/user_favorite');
    ref.on(
      'value',
      function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var childData = childSnapshot.val();
          //console.log(childSnapshot.val().date)
          if (childSnapshot.val().user_id == that.state.Uid) {
            arr.push(childSnapshot.val());
            //   console.log(childSnapshot.val().exersises_name)
          }
        });
        that.setState({exersises: arr});
        console.log('exercise', that.state.exersises);
      },
      function(error) {
        console.log('Error:' + error.code);
      },
    );
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <View style={styles.mainContainer}>
          <View style={styles.list}>
            <FlatList
              data={this.state.exersises}
              style={{width: '100%', height: '100%'}}
              keyExtractor={item => item.user_id}
              renderItem={({item}) => {
                return (
                  <View style={styles.exerciseListItem}>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{...styles.row, width: '70%', alignContent: 'center',
                        justifyContent: 'center',}}>
                          <View
                            style={{
                              alignContent: 'center',
                              justifyContent: 'center',
                            }}>
                            <Text style={styles.title}>{item.exersises_name}</Text>
                          </View>
                         
                      </View>
                      <View
                        style={{
                          ...styles.exerciseQRContainer,
                          width: '30%',
                          alignItems: 'center',
                          height: '100%',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigation.navigate('QRScanner'
                            );
                          }}>
                          <Image
                            style={styles.qrIcon}
                            source={require('../../assets/images/qr_code.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        </View>
      </ImageBackground>
    );
  }
}
Favorites.navigationOptions = navData => {
  return {
    headerTitle: 'Your Favorites',
  };
};
const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
    marginBottom: '-22%',
  
  },
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noFavoritesContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
  },
  list: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  noFavoritesText: {
    color: '#fff',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  exerciseListItem: {
    height: 100,
    width: '100%',
    backgroundColor: '#fff',
    //borderRadius: 10,
    overflow: 'hidden',
    marginVertical: '1%',
    justifyContent: 'center',
    alignContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  exerciseListImage: {
    width: 60,
    height: 60,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  exerciseQRContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  exerciseListTitleContainer: {
    paddingVertical: 25,
    flex: 1,
  },
  title: {
    fontSize: 24,
    color: Colors.textColor,
    textAlign: 'left',
    
  },
  title1: {
    fontSize: 30,
    color: Colors.textColor,
    marginTop: 20,
  },
  qrIcon: {
    width: 60,
    height: 60,
  },
});
