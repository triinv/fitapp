import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  PixelRatio,
  Platform,
  Dimensions,
  ImageBackground,
} from 'react-native';
import YouTube from 'react-native-youtube';
import {Button} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import firebase from '@react-native-firebase/app';
import Colors from '../../assets/Colors.js';

const getVideoId = require('get-video-id');
let id = '';
var array = [];
export default class Exercise extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      videos: JSON.stringify(this.props.navigation.state.params.video),
      info: JSON.stringify(this.props.navigation.state.params.info),
      exer: JSON.stringify(this.props.navigation.state.params.exer),
      tips: JSON.stringify(this.props.navigation.state.params.tips),
      Array: this.props.navigation.state.params.data,
      isReady: false,
      status: null,
      quality: null,
      error: null,
      isPlaying: true,
      isLooping: false,
      duration: 0,
      currentTime: 0,
      fullscreen: false,
      playerWidth: Dimensions.get('window').width,
      Uid: '',
      video: '',
      data: {},
    };
  }

  _youTubeRef = React.createRef();

  handleBackButtonClick() {
    // We can move to any screen. If we want
    this.props.navigation.navigate('Dashboard');
    // Returning true means we have handled the backpress
    // Returning false means we haven't handled the backpress
    return true;
  }

  componentDidMount() {
    console.log('dat videoe', this.state.videos);
    id = getVideoId(JSON.parse(this.state.videos)).id;
    console.log('VID info', this.state.info);
    console.log('VID exer video...', this.state.exer);
    console.log('VID exer videoDDDDEE...', this.state.videos);
    console.log('VID exer videoDD...', this.state.tips);
    this.props.navigation.setParams({add: this.add});
    auth().onAuthStateChanged(user => {
      this.setState({
        Uid: user.uid,
      });
    });
  }

  add = () => {
    var that = this;
    firebase
      .database()
      .ref('/user_favorite')
      .push({
        exersises_name: JSON.parse(that.state.exer),
        user_id: that.state.Uid,
      });
    alert('Added to favorites');
  };
  renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );

  render() {
    const YOUR_API_KEY = 'AIzaSyDp0DN2_Gv33RobuXg8C8dju-yieiLMnOk';
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <ScrollView style={{flex: 1}}>
          <View style={styles.mainContainer}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.textColor,
                height: 50,
                marginBottom: -10,
                width: Dimensions.get('window').width,
              }}>
              <Text style={styles.header}>{JSON.parse(this.state.exer)}</Text>
            </View>
            <YouTube
              ref={this._youTubeRef}
              apiKey={YOUR_API_KEY}
              videoId={id}
              play={false}
              loop={false}
              fullscreen={false}
              controls={1}
              style={[
                {
                  height: PixelRatio.roundToNearestPixel(
                    this.state.playerWidth / (16 / 9),
                  ),
                },
                styles.player,
              ]}
              onError={e => {
                this.setState({error: e.error});
              }}
              onReady={e => {
                this.setState({isReady: true});
              }}
              onChangeState={e => {
                this.setState({status: e.state});
              }}
              onChangeQuality={e => {
                this.setState({quality: e.quality});
              }}
              onChangeFullscreen={e => {
                this.setState({fullscreen: e.isFullscreen});
              }}
              onProgress={e => {
                this.setState({currentTime: e.currentTime});
              }}
            />
            <View style={{width: '92%', marginBottom: '5%'}}>
              <View
                style={{
                  borderBottomColor: '#fff',
                  borderBottomWidth: 1,
                  paddingTop: '5%',
                }}>
                <Text style={styles.title}>Description</Text>
              </View>
              <Text style={styles.text}>{JSON.parse(this.state.info)}</Text>

              <View
                style={{
                  borderBottomColor: '#fff',
                  borderBottomWidth: 1,
                  paddingTop: '5%',
                }}>
                <Text style={styles.title}>Tips</Text>
              </View>
              <Text style={styles.text}>{JSON.parse(this.state.tips)}</Text>
            </View>

            {this._youTubeRef.current &&
              this._youTubeRef.current.props.videoIds &&
              Array.isArray(this._youTubeRef.current.props.videoIds) && (
                <View style={styles.buttonGroup}>
                  {this._youTubeRef.current.props.videoIds.map((videoId, i) => (
                    <React.Fragment key={i}>
                      <Button
                        title={`Video ${i}`}
                        onPress={() => {
                          if (this._youTubeRef.current) {
                            this._youTubeRef.current.playVideoAt(i);
                          }
                        }}
                      />
                      <Text />
                    </React.Fragment>
                  ))}
                </View>
              )}
          </View>
          {/* Reload iFrame for updated props (Only needed for iOS) */}
        </ScrollView>
      </ImageBackground>
    );
  }
}
Exercise.navigationOptions = ({navigation, screenProps}) => {
  return {
    headerTitle: 'Instructions',
    headerBackTitle: null,
    headerRight: (
      <Button
        icon={<Icon name="star" size={30} color="#fff" />}
        title={''}
        type={'clear'}
        onPress={navigation.getParam('add')}
      />
    ),
  };
};
const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    paddingTop: Platform.OS === 'android' ? '14%' : '24%',
  },
  container: {
    backgroundColor: '#fff',
  },
  header: {
    fontSize: 16,
    textAlign: 'center',
    color: '#fff',
    fontWeight: '600',
  },
  headerContainerStyle: {
    flexDirection: 'row',
    backgroundColor: '#0062b1',
    justifyContent: 'space-between',
    padding: 15,
  },
  buttonGroup: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingBottom: 5,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  player: {
    alignSelf: 'stretch',
    marginVertical: 10,
  },
  youtube: {
    alignSelf: 'stretch',
    height: 300,
  },
  title: {
    color: '#fff',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'flex-start',
    padding: 5,
  },
  text: {
    color: '#fff',
    fontSize: 16,
    justifyContent: 'flex-start',
    padding: 10,
  },
});
