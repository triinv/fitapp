import React, {useState, useEffect} from 'react';
import {
  ImageBackground,
  StyleSheet,
  View,
  Text,
  Dimensions,
  Picker,
} from 'react-native';
import Colors from '../../assets/Colors';
import DatePicker from 'react-native-datepicker';
import {Button} from 'react-native-elements';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

const AddActivity = props => {
  function onAuthStateChanged(user) {
    setUser(user.uid);
  }
  const [user, setUser] = useState('');
  const [date, setDate] = useState('');
  const [selected_exersise, setSelected_exersise] = useState('');
  const [exersises, setExersises] = useState([
    'Select exercises',
    'Abs & Core',
    'Arms',
    'Back',
    'Chest',
    'Lower Body',
    'Shoulders',
    'Endurance & Cardio',
    'Mobility',
    'Crossfit',
    'HIIT',
    'Strength',
  ]);

  useEffect(() => {
    get_parent();
    auth().onAuthStateChanged(onAuthStateChanged);
    console.log('userId', {user});
  }, [get_parent, user]);

  function Add() {
    firebase
      .database()
      .ref('/activity_calender')
      .push({
        exersises_name: selected_exersise,
        date: date,
        user_id: user,
      });
    alert('Added successfully');
    props.navigation.navigate('Activity');
  }
  function getParent(snapshot) {
    // var ref = snapshot.ref()
    var key = Object.keys(snapshot.val());
    setExersises(key);
  }
  function get_parent() {
    firebase
      .database()
      .ref('/exercise_names')
      .once('value', function(snapshot) {
        // var childKey = snapshot.child('exercise_names');
        console.log('Name of the parent: ' + getParent(snapshot));
      });
  }
  return (
    <ImageBackground
      style={styles.backgroundImage}
      source={require('../../assets/images/fitapp_bg.png')}>
      <View style={styles.mainContainer}>
        <View style={styles.middleContainer}>
          <View
            style={[
              styles.pickerRow,
              {borderColor: '#ccc', borderBottomWidth: 1},
            ]}>
            <View style={styles.pickerTitleContainer}>
              <Text style={styles.placeHolderText}>Select Date</Text>
            </View>
            <View style={styles.pickerContainer}>
              <DatePicker
                style={{width: '100%', color: Colors.textColor}}
                date={date}
                mode="date"
                placeholder="Select date"
                format="YYYY-MM-DD"
                minDate="2020-01-01"
                maxDate="2050-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    display: 'none',
                  },
                  placeholderText: {
                    color: Colors.textColor,
                    fontSize: 16,
                  },
                  dateInput: {
                    borderColor: 'transparent',
                    borderBottomWidth: 1,
                    borderBottomColor:
                      Platform.OS === 'android' ? '#ccc' : 'transparent',
                  },
                  btnTextConfirm: {
                    color: Colors.primaryColor,
                  },
                }}
                onDateChange={date => {
                  setDate(date);
                  console.log('date picker isss', date);
                }}
              />
            </View>
          </View>
          <View style={styles.pickerRow}>
            <View style={styles.pickerTitleContainer}>
              <Text style={styles.placeHolderText}>Select Exersise</Text>
            </View>
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={selected_exersise}
                style={styles.picker}
                itemStyle={styles.pickerItem}
                onValueChange={(itemValue, itemIndex) => {
                  setSelected_exersise(itemValue);
                  console.log('exer', itemValue);
                }}>
                {exersises.map((item, index) => {
                  return <Picker.Item label={item} value={item} key={index} />;
                })}
              </Picker>
            </View>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <Button
            buttonStyle={styles.loginButtonStyle}
            titleStyle={styles.buttonText}
            title="Save"
            onPress={() => Add()}
          />
        </View>
      </View>
    </ImageBackground>
  );
};
AddActivity.navigationOptions = navData => {
  return {
    headerTitle: 'Calander',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    paddingTop: '22%',
  },
  middleContainer: {
    flex: 2,
    width: '95%',
    marginTop: Platform.OS === 'android' ? '0%' : '5%',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#fff',
    //borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  pickerRow: {
    flex: 1,
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickerTitleContainer: {
    flex: 1,
  },
  placeHolderText: {
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    color: Colors.textColor,
    fontSize: 18,
  },
  pickerContainer: {
    flex: 2,
  },
  picker: {
    height: 100,
  },
  pickerItem: {
    height: 100,
    color: Colors.textColor,
    fontSize: 16,
    marginHorizontal: '2%',
  },
  bottomContainer: {
    flex: 2,
    justifyContent: 'center',
  },
  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
});

export default AddActivity;
