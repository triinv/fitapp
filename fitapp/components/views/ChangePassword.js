import React from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  Alert,
  ImageBackground,
  Dimensions,
  Platform,
} from 'react-native';
import {Button} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import Colors from '../../assets/Colors';
import PasswordInputText from 'react-native-hide-show-password-input';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      Password: '',
    };
  }
  changePassword() {
    var that = this;
    var user = auth().currentUser;
    user
      .updatePassword(this.state.email)
      .then(function() {
        Alert.alert('Password was changed');
        that.props.navigation.navigate('Dashboard');
      })
      .catch(function(e) {
        Alert.alert(e);
      });
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_simple_bg.png')}>
        <View style={styles.mainContainer}>
          <View style={styles.middleContainer}>
            <View style={{width: '80%'}}>
              <PasswordInputText
                id="newPassword"
                label="New Password"
                keyboardType="default"
                secureTextEntry
                required
                minLength={5}
                autoCapitalize="none"
                errorText="Please enter new password."
                //onInputChange={inputChangeHandler}
                //initialValue=""
                onChangeText={value => this.setState({email: value})}
                textColor={Colors.textColor}
                tintColor={Colors.textColor}
                baseColor={Colors.textColor}
                labelTextStyle={styles.label}
                inputStyle={styles.input}
                leftIcon={{
                  type: 'font-awesome',
                  name: 'lock',
                  color: Colors.textColor,
                  marginLeft: -20,
                  padding: '5%',
                }}
              />
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
              <Button
                buttonStyle={styles.buttonStyle}
                title="Save"
                onPress={() => this.changePassword()}
              />
            </SafeAreaView>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

ChangePassword.navigationOptions = navData => {
  return {
    headerTitle: 'Change password',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
  },
  middleContainer: {
    marginTop: 20,
    width: '95%',
    height: 210,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    //borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  bottomContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  label: {
    color: Colors.textColor,
    textTransform: 'uppercase',
    fontWeight: '400',
  },
  input: {
    color: Colors.textColor,
    fontSize: 16,
  },
});

export default ChangePassword;
