import {
  StyleSheet,
  FlatList,
  ImageBackground,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

const Categories = props => {
  const [user, setUser] = useState('');
  const [image, setImage] = useState('');
  const [selected_exersise, setSelected_exersise] = useState('');
  const [exersises, setExersises] = useState(['select exercises']);
  const renderGridItem = itemData => {
    return (
      <TouchableOpacity
        style={styles.gridItem}
        onPress={() => get_single(itemData.item.name)}>
        <ImageBackground
          source={{uri: itemData.item.image_url}}
          style={styles.gridContainer}
          imageStyle={{borderRadius: 10}}>
          <Text style={styles.title}>{itemData.item.name}</Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  };
  useEffect(() => {
    get_firebase();
    auth().onAuthStateChanged(user => {
      setUser(user.uid);
    });
  }, [get_firebase]);
  function get_firebase() {
    var array = [];
    var img = [];
    var ref = firebase.database().ref('exercise_name');
    ref.on(
      'value',
      function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var childData = childSnapshot.val();
          var data = childSnapshot.val();
          // var key = Object.keys(snapshot);
          var key = childSnapshot.key;
          img.push({name: key, image_url: data.image_url});
        });
        setImage(img);
        console.log('image', image);
      },
      function(error) {
        console.log('Error:' + error.code);
      },
    );
  }
  function get_single(name) {
    props.navigation.navigate('ExerciseCategory', {data: name});
  }
  return (
    <ImageBackground
      style={styles.backgroundImage}
      source={require('../../assets/images/fitapp_simple_bg.png')}>
      <View style={styles.categoriesMainContainer}>
        <FlatList
          keyExtractor={(item, index) => item.id}
          data={image}
          renderItem={renderGridItem}
          numColumns={1}
        />
      </View>
    </ImageBackground>
  );
};

Categories.navigationOptions = navData => {
  return {
    headerTitle: 'Browse by',
    headerBackTitle: null,
  };
};
const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoriesMainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
    marginBottom: '-22%',
  },
  gridItem: {
    flex: 1,
    margin: 15,
    height: 150,
  },
  gridContainer: {
    flex: 1,
    borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    padding: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  title: {
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
  },
});

export default Categories;
