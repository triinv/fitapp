import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  View,
  Dimensions,
  Platform,
} from 'react-native';
import Colors from '../../assets/Colors';
import {Button} from 'react-native-elements';
import auth from '@react-native-firebase/auth';
import {TextField} from 'react-native-material-textfield';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      Password: '',
    };
  }

  forgotPassword() {
    auth()
      .sendPasswordResetEmail(this.state.email)
      .then(function(user) {
        alert(
          'Check your email address and see the instructions on how to proceed',
        );
      })
      .catch(error => {
        alert('Please enter a valid e-mail address');
      });
  }

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('../../assets/images/fitapp_bg.png')}>
        <View style={styles.mainContainer}>
          <View style={styles.middleContainer}>
            <View style={{width: '80%'}}>
              <TextField
                id="email"
                label="Email"
                keyboardType="email-address"
                required
                email
                textColor={Colors.textColor}
                tintColor={Colors.textColor}
                baseColor={Colors.textColor}
                labelTextStyle={styles.label}
                inputStyle={styles.input}
                autoCapitalize="none"
                errorText="Please enter a valid email address."
                //onInputChange={inputChangeHandler}
                initialValue=""
                onChangeText={value => this.setState({email: value})}
                leftIcon={{
                  type: 'font-awesome',
                  name: 'user',
                  color: Colors.textColor,
                  marginLeft: -20,
                  padding: '5%',
                }}
              />
            </View>
          </View>
          <View style={styles.bottomContainer}>
            <View style={styles.buttonContainer}>
              <Button
                buttonStyle={styles.loginButtonStyle}
                titleStyle={styles.buttonText}
                title="Reset password"
                onPress={() => this.forgotPassword()}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

ForgotPassword.navigationOptions = navData => {
  return {
    headerTitle: 'Forgot Password',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
  },
  middleContainer: {
    marginTop: 20,
    width: '80%',
    height: 210,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    //borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  bottomContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonStyle: {
    height: 60,
    width: '100%',
    borderRadius: 30,
    backgroundColor: Colors.accentColorYellow,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: '500',
    textTransform: 'uppercase',
    color: Colors.primaryColor,
  },
  loginOptionButtonStyle: {
    width: 300,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    marginBottom: 20,
  },
  loginOptionButtonTextStyle: {
    fontSize: 16,
  },
  label: {
    color: Colors.textColor,
    textTransform: 'uppercase',
    fontWeight: '400',
  },
  input: {
    color: Colors.textColor,
    fontSize: 16,
    backgroundColor: 'red',
  },
});

export default ForgotPassword;
