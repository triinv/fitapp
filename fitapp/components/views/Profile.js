import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Colors from '../../assets/Colors';
import {userData} from '../../redux/actions/auth';

const Profile = props => {
  let {username, actions} = props;

  return (
    <ImageBackground
      style={styles.backgroundImage}
      source={require('../../assets/images/fitapp_simple_bg.png')}>
      <View style={styles.mainContainer}>
        <View style={styles.middleContainer}>
          <View style={styles.iconInput}>
            <Icon name="email" size={25} color={Colors.textColor} />
            <Text style={styles.inputText}>{username}</Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate({
                routeName: 'ChangePassword',
              });
            }}
            style={styles.profileTouchable}>
            <View style={styles.row}>
              <Icon name="lock" size={25} color={Colors.textColor} />
              <Text style={styles.inputText}>Change Password</Text>
            </View>
            <Icon
              name="keyboard-arrow-right"
              size={30}
              color={Colors.textColor}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.profileButtonContainer}>
          <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
            <Button
              buttonStyle={styles.clearButton}
              title="Logout"
              titleStyle={{color: Colors.accentColorRed}}
              type={'clear'}
              onPress={() => {
                props.navigation.navigate('Login');
              }}
            />
          </SafeAreaView>
        </View>
      </View>
    </ImageBackground>
  );
};

Profile.navigationOptions = navData => {
  return {
    headerTitle: 'Profile',
    headerBackTitle: null,
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    paddingTop: Platform.OS === 'android' ? '12%' : '22%',
  },
  middleContainer: {
    marginTop: 20,
    width: '95%',
    height: 160,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  row: {
    flexDirection: 'row',
  },
  clearButton: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderContainer: {
    backgroundColor: Colors.primaryColor,
    width: '100%',
    paddingVertical: '10%',
    alignItems: 'center',
  },
  avatarContainer: {
    backgroundColor: '#fff',
    borderRadius: 100,
    width: 75,
    height: 75,
  },
  iconInput: {
    paddingVertical: 20,
    width: '90%',
    flexDirection: 'row',
  },
  profileTouchable: {
    borderColor: '#ccc',
    borderTopWidth: 1,
    paddingVertical: 20,
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  profileBottomTouchable: {
    paddingVertical: 20,
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputText: {
    paddingLeft: '2%',
    alignSelf: 'center',
    color: Colors.textColor,
  },
  profileButtonContainer: {
    marginTop: 20,
    width: '95%',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
});

const mapStateToProps = function(state) {
  return {
    username: state.username.username,
  };
};
const ActionCreators = Object.assign({}, {userData});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
