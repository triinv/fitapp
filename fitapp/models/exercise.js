class Exercise {
    constructor(
        id,
        categoryIds,
        title,
        imageUrl,
        machineImageUrl,
        video,
        machineTitle,
        machineNr,
        instructions,
        tips
    ) {
        this.id = id;
        this.categoryIds = categoryIds;
        this.title = title;
        this.imageUrl = imageUrl;
        this.machineImageUrl = machineImageUrl;
        this.video = video;
        this.machineTitle = machineTitle;
        this.machineNr = machineNr;
        this.instructions = instructions;
        this.tips = tips;

    }
}


export default Exercise;

