import { CHANGE_LANG } from '../actions/lang';
import { setLocale, getCurrentLocale } from '../../utils/language.utils';

const initialState = {language: getCurrentLocale()};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_LANG:
            setLocale(action.payload);
            return Object.assign({}, state, {
                language: action.payload
            })

        default:
            return state;
    }
};

export default rootReducer;

