import {EXERCISES} from '../../data/dummy-data.js';
import {ADD_FAVORITE} from '../actions/exercises';

const initialState = {
    exercises: EXERCISES,
    favoriteExercises: [],
};

const exerciseReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_FAVORITE:
            const existingIndex = state.favoriteExercises.findIndex(
                exercise => exercise.id === action.exerciseId,
            );
            if (existingIndex >= 0) {
                const updatedFavoritedExercises = [...state.favoriteExercises];
                updatedFavoritedExercises.splice(existingIndex, 1);
                return {...state, favoriteExercises: updatedFavoritedExercises};
            } else {
                const exercise = state.exercises.find(exercise => exercise.id === action.exerciseId);
                return {...state, favoriteExercises: state.favoriteExercises.concat(exercise)};
            }
        default:
            return state;
    }
};

export default exerciseReducer;
