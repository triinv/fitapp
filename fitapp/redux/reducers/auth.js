import {AUTHENTICATE, LOGOUT, USER_NAME, USER_ID} from '../actions/auth';

const initialState = {
  token: null,
  userId: null,
  username: '',
  userid: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return {
        token: action.token,
        userId: action.userId,
      };
    case LOGOUT:
      return {
        token: action.token,
        userId: action.userId,
      };
    // case SIGNUP:
    //   return {
    //     token: action.token,
    //     userId: action.userId
    //   };
    case USER_NAME:
      return {
        ...state,
        username: action.payload,
      };
    case USER_ID:
      return {
        ...state,
        userid: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
