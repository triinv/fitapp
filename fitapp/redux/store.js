import { createStore, combineReducers } from 'redux';
import authReducer from './reducers/auth.js';
const rootReducer = combineReducers(
  {  username: authReducer,
  }
);
const configureAuth = () => {
  return createStore(rootReducer);
}
export default configureAuth;
