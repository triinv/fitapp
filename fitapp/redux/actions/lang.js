export const CHANGE_LANG = "CHANGE_LANG";

export const changeLanguage = lang => (
    {
        type: CHANGE_LANG,
        payload: lang,
    }
);
