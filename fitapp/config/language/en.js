
export default {
    favoriteExercise: 'Your favourite exercises',

    monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
    monthNamesShort: ['Jan.','Feb.','Mar','Apr','May','Jun','Jul.','Aug','Sept.','Oct.','Nov.','Dec.'],
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Mon.','Tue.','Wed.','Thu.','Fri.','Sat.','Sun.'],
    today: 'Today\'Today'

}


