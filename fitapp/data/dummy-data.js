import Category from '../models/category';
import Exercise from '../models/exercise';


export const CATEGORIES = [
    new Category('c1', 'Lower Body','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/spotive-man-and-woman-lifting-heavy-barbells-royalty-free-image-1569601646.jpg',),
    new Category('c2', 'Abs & Core', 'https://www.t-nation.com/system/publishing/article_assets/5129/original/Incorrect.jpg?ts=1495658061'),
    new Category('c3', 'Chest', 'https://cdn2.coachmag.co.uk/sites/coachmag/files/styles/16x9_480/public/2016/10/physique_.jpg?itok=9wPjWJPh&timestamp=1475765357'),
    new Category('c4', 'Shoulders', 'https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2018-6/Back_Dumbbell_Workout_Weights-1296x728-Header.jpg?w=1155'),
    new Category('c5', 'Back', 'https://cdn.shopify.com/s/files/1/0866/7664/articles/Lower_Back_Exercises_2048x.jpg?v=1535038808'),
    new Category('c6', 'Arms', 'https://post.greatist.com/wp-content/uploads/2019/05/Male_Bicep_Curl_1200x628-facebook.jpg'),
    new Category('', '', ''),


];


export const EXERCISES = [
    new Exercise(
        'ex1',
        ['c1'],
        'Incline leg press',
        'https://weighttraining.guide/wp-content/uploads/2016/05/Sled-45-degree-Leg-Press.png',
'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpQACrM4sNveai0h-AdvIAvWdzdC9ChPmgfWpV9b0eVUujZ8wB&usqp=CAU',
        'https://youtu.be/xCQ-FY_bj9E',
        'Leg press machine',
        12,
        [
            'Sit in the leg press machine. Make sure your back is pressed firmly against the back support.',
            'Place your feet hip-width apart on the platform.',
            'Release the safety bars, grasp the side handles for support, and extend your legs without locking your knees. Your torso and legs should make a 90-degree angle.',
            'Inhale as you bend your legs and lower the platform until your knees are almost fully flexed.',
            'Driving with your heels, press the platform back up to the starting position as you exhale.',
            'When you have finished, lock the safety bars properly.'
        ],
        [
            'Make sure that the platform is securely locked when you load the weight.',
            'Keep your feet flat on the platform, and your knees and toes pointing in the same direction (slightly outward).',
            'Do not lock your knees out when you press the platform up.',
            'As you lower the platform, do not allow your lower back to curl up; keep it in firm contact with the support.'
        ],

    ),
    new Exercise(
        'ex2',
        ['c1'],
        'Sled calf press',
        'https://weighttraining.guide/wp-content/uploads/2016/12/sled-calf-press-990x488.png',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpQACrM4sNveai0h-AdvIAvWdzdC9ChPmgfWpV9b0eVUujZ8wB&usqp=CAU',
        'https://youtu.be/M4FojyRAcuE',
        'Leg press machine',
        12,
        [
            'Sit in the sled/leg press machine with your back pressed against the back support.',
            'Place your feet flat in the center of the platform.',
            'Push the platform up until your knees are almost fully extended.',
            'Position the balls of your feet on the bottom edge of the platform so that the arches and heels of your feet extend off.',
            'Exhale as you extend your ankles to push the platform up.',
            'Hold for a count of two and squeeze your calves.',
            'Inhale as you flex your ankles to lower the platform until you feel a mild stretch in your calves.',
            'Hold for a second to allow the stored elastic energy in your calves to dissipate.',
            'Repeat the press.'
        ],
        [
            'Keep both phases of the repetition slow.',
            'Try not to lock out your knees.',
            'If you have a difference in muscle size or strength between your two calves, do one calf at a time, starting with your weak calf and making sure to not perform more repetitions with your strong calf.',
        ],
    ),
    new Exercise(
        'ex3',
        ['c2'],
        'Decline crunch',
        'https://weighttraining.guide/wp-content/uploads/2018/11/Decline-crunch-990x531.png',
        'https://4.imimg.com/data4/UQ/BE/MY-11248407/olympic-decline-bench-500x500.jpg',
        'https://youtu.be/fnERlUHmsy4',
        'Decline bench',
        39,
        [
            'Lie on your back (supine) on a declined bench and hook your feet under the rolls.',
            'Position your hands either across your chest or behind your head or neck.',
            'To protect your lower back, press it down, against the bench.',
            'Keeping your neck neutral and lower back pressed against the bench, exhale as you raise your head and shoulders off the bench by flexing your abdomen.',
            'Hold for a count of two.',
            'Inhale as you lower your head and shoulders back to the starting position and relax your abdomen.',
            'Repeat for the prescribed number of repetitions.'
        ],
        [
            'Keep your neck neutral; there should be lots of space between your chin and sternum.',
            'Don’t allow any momentum to build up; keep the movement slow and under control.',
            'Make the decline crunch more difficult by keeping your arms stretched up, or by holding a weight plate either on your chest or behind your head.',
        ],
    ),

];

