
// import {AppRegistry} from 'react-native';
// import App from './components/App.js';
// import {name as appName} from './app.json';


// AppRegistry.registerComponent(appName, () => App);
import { AppRegistry } from 'react-native';
import React from 'react';
import Routes from './navigation/Routes';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import { name as appName } from './app.json';
import configureAuth from './redux/store.js';
console.disableYellowBox = true;
const store = configureAuth()
const RNRedux = () => (
  <Provider store = { store }>
    <Routes/>
  </Provider>
)
AppRegistry.registerComponent(appName, () => RNRedux);
